create database if not exists flight_db;
use flight_db;


create table plane_type (
	id int not null auto_increment primary key,
	name varchar(45) not null
);

create table plane (
	id int not null auto_increment primary key,
    name varchar(45) not null,
    chair_amout int not null,
    plane_type_id int not null,
  
	index FK_plane_plane_type_idx (plane_type_id asc),
  
	constraint FK_plane_plane_type
    foreign key (plane_type_id)
    references plane_type (id)
    on delete no action
    on update no action
);

create table client (
	id int not null auto_increment primary key,
    name varchar(45) not null,
    birth_date date not null,
    city varchar(45) not null,
    rating varchar(5)
);

create table book (
	id int not null auto_increment primary key,
    date datetime not null,
    place varchar(10) not null,
    plane_id int not null,
    client_id int not null,
  
	index FK_book_plane_idx (plane_id asc),
	index FK_book_client_idx (client_id asc),
  
	constraint FK_book_plane
    foreign key (plane_id)
    references plane (id)
    on delete no action
    on update no action,
    constraint FK_book_client
    foreign key (client_id)
    references client (id)
    on delete no action
    on update no action
);

create table flight (
	id int not null auto_increment primary key,
    duration int not null,
    destination varchar(100) not null,
    book_id int not null,
    
    index FK_flight_book_idx (book_id asc),
    
    constraint FK_flight_book
    foreign key (book_id)
    references book (id)
    on delete no action
    on update no action
);